# 1.介绍

本项目是[tinyriscv](https://gitee.com/liangkangnan/tinyriscv)在Xilinx FPGA平台上运行的实例。

# 2.平台

- 硬件：Xilinx Artix-7 XC7A35T
- 开发环境：Vivado 2018.1

# 3.步骤(通过JTAG在线更新程序)

1.将综合、实现后生成的bit文件下载到FPGA里。

2.将CMSIS-DAP调试器连接到FPGA，然后打开一个CMD窗口运行openocd服务：

`openocd.exe -f tinyriscv.cfg`

3.再打开另一个CMD窗口连接到openocd server，即运行：

`telnet localhost 4444`

4.通过load_image命令将bin程序下载到FPGA，比如：

`load_image example.bin 0x0 bin 0 100000`

5.通过verify_image命令校验下载是否成功，比如：

`verify_image example.bin 0x0`

6.最后输入reset命令即可让下载的程序跑起来。

`reset`

# 4.其他

所需的openocd.exe和tinyriscv.cfg文件在[tinyriscv](https://gitee.com/liangkangnan/tinyriscv)的tools/openocd目录里。

